API STORE

this project has this structure:

ewtest \ store

| _ views.py (application controller)

| _ services.py (has application business rules)

| _ serializers.py (performs serialization of JSON to send to the model)

| _ tests.py (some automated tests)

ewtest \ store \ models

| _ models.py (all application models)

| _ manager.py (classes and methods for implementing the logical deletion)



Tools used:

Django rest framework

Postgresql

NGINX

Gunicorn

Hosted on AWS EC2


Endpoints

http://18.230.155.104/store/api/v1/user/ (method POST)

JSON example:

body:

{
  "password": "1234",
  "email": "user5@test.com",
  "client": {
      "name": "Carlos",
      "address": "Street",
      "complement": "Complement",
      "city": "Christmas",
      "state": "RN",
      "phone": "84994032362"
  }
}

http://18.230.155.104/store/api/v1/login/ (method POST)

JSON example:

body:

{
  "password": "1234",
  "email": "user5@test.com"
}

http://18.230.155.104/store/api/v1/products/

http://18.230.155.104/store/api/v1/products/{id]
 (all methods implemented)
 
JSON example:

header:

Authorization Token [valid token]

body:

{
  "description": "PRODUCT 3",
  "abstract": "PRODUCT 3 test",
  "value": 15,
  "stock": 0
}


http://18.230.155.104/store/api/v1/sale/
 (POST and GET methods)


JSON example:

header:

Authorization Token [valid token]

body:

{
    "client": [id client valid],
    "comments": "",
    "details": [
        {
            "quantity": 1,
            "product": [id product valid],
            "value": 10,
            "comments": ""
        }
    ]
}