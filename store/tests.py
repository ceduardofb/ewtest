from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status
from django.urls import reverse

from rest_framework.authtoken.models import Token
from .models.models import Product, Client


class UserTestCase(TestCase):
    """Test suite for the api views."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.client = APIClient()

        self.user_data = {"password": "1234", "email": "user4@teste.com",
                          "client": {
                              "name": "Carlos",
                              "address": "Rua",
                              "complement": "Complemento",
                              "city": "Natal",
                              "state": "RN",
                              "phone": "84994032362"
                            }
                          }

        self.response = self.client.post(
            reverse('user_list'),
            self.user_data,
            format="json")

    def test_api_can_create(self):
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)


class LoginTestCase(TestCase):
    """Test suite for the api views."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.client = APIClient()

        self.user_data = {"password": "1234", "email": "user4@teste.com",
                          "client": {
                              "name": "Carlos",
                              "address": "Rua",
                              "complement": "Complemento",
                              "city": "Natal",
                              "state": "RN",
                              "phone": "84994032362"
                            }
                          }

        self.response = self.client.post(
            reverse('user_list'),
            self.user_data,
            format="json")

        self.user_data = {"password": "1234", "email": "user4@teste.com"}

        self.response = self.client.post(
            reverse('login'),
            self.user_data,
            format="json")

    def test_api_can_login(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)


class ProductTestCase(TestCase):
    """Test suite for the api views."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.client = APIClient()

        self.user_data = {"password": "1234", "email": "user4@teste.com",
                          "client": {
                              "name": "Carlos",
                              "address": "Rua",
                              "complement": "Complemento",
                              "city": "Natal",
                              "state": "RN",
                              "phone": "84994032362"
                          }
                          }

        self.response = self.client.post(
            reverse('user_list'),
            self.user_data,
            format="json")

        self.user_data = {"password": "1234", "email": "user4@teste.com"}

        self.response = self.client.post(
            reverse('login'),
            self.user_data,
            format="json")

        token = Token.objects.get(user__email="user4@teste.com")

        self.user_data = {"description": "PRODUCT ", "abstract": "PRODUCT  test", "value": 15, "stock": 0}
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Token ' + str(token),
        }

        self.response = self.client.post(
            reverse('products_list'),
            self.user_data,
            format="json", **auth_headers)

    def test_api_can_create(self):
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)


class SaleTestCase(TestCase):
    """Test suite for the api views."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.client = APIClient()

        self.user_data = {"password": "1234", "email": "user4@teste.com",
                          "client": {
                              "name": "Carlos",
                              "address": "Rua",
                              "complement": "Complemento",
                              "city": "Natal",
                              "state": "RN",
                              "phone": "84994032362"
                          }
                          }

        self.response = self.client.post(
            reverse('user_list'),
            self.user_data,
            format="json")

        self.user_data = {"password": "1234", "email": "user4@teste.com"}

        self.response = self.client.post(
            reverse('login'),
            self.user_data,
            format="json")

        self.token = Token.objects.get(user__email="user4@teste.com")

        self.product, created = Product.objects.get_or_create(description="PRODUCT", abstract="PRODUCT  test",
                                                              value=15, stock=0)

        self.customer = Client.objects.get(user__email="user4@teste.com")

        self.user_data = {"client": self.customer.id, "seller": "",  "comments": "Comments", "details": [{"quantity": 1,
                                                                                         "product": self.product.id,
                                                                                         "value": 10,
                                                                                         "comments": ""}],
                          "total_value": 10}

        self.auth_headers = {
            'HTTP_AUTHORIZATION': 'Token ' + str(self.token),
        }

    def test_api_can_create(self):

        self.user_data = {"client": self.customer.id, "seller": "",  "comments": "Comments", "details": [{"quantity": 1,
                                                                                         "product": self.product.id,
                                                                                         "value": 10,
                                                                                         "comments": ""}],
                          "total_value": 10}

        auth_headers = {
            'HTTP_AUTHORIZATION': 'Token ' + str(self.token),
        }

        self.response = self.client.post(
            reverse('sale_list'),
            self.user_data,
            format="json", **auth_headers)

        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_api_client_not_exists(self):

        self.user_data = {"client": 25, "seller": "",  "comments": "Comments", "details": [{"quantity": 1,
                                                                                         "product": self.product.id,
                                                                                         "value": 10,
                                                                                         "comments": ""}],
                          "total_value": 10}

        self.response = self.client.post(
            reverse('sale_list'),
            self.user_data,
            format="json", **self.auth_headers)

        self.assertEqual(self.response.status_code, status.HTTP_404_NOT_FOUND)

    def test_api_client_none(self):

        self.user_data = {"client": None, "seller": "",  "comments": "Comments", "details": [{"quantity": 1,
                                                                                         "product": self.product.id,
                                                                                         "value": 10,
                                                                                         "comments": ""}],
                          "total_value": 10}

        self.response = self.client.post(
            reverse('sale_list'),
            self.user_data,
            format="json", **self.auth_headers)

        self.assertEqual(self.response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_api_client_blank(self):
        self.user_data = {"client": "", "seller": "", "comments": "Comments", "details": [{"quantity": 1,
                                                                                             "product": self.product.id,
                                                                                             "value": 10,
                                                                                             "comments": ""}],
                          "total_value": 10}

        self.response = self.client.post(
            reverse('sale_list'),
            self.user_data,
            format="json", **self.auth_headers)

        self.assertEqual(self.response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_api_total_diverge(self):
        self.user_data = {"client": self.customer.id, "seller": "", "comments": "Comments", "details": [{"quantity": 1,
                                                                                             "product": self.product.id,
                                                                                             "value": 10,
                                                                                             "comments": ""}],
                          "total_value": 5}

        self.response = self.client.post(
            reverse('sale_list'),
            self.user_data,
            format="json", **self.auth_headers)

        self.assertEqual(self.response.status_code, status.HTTP_400_BAD_REQUEST)