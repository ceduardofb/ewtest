from django.contrib.auth.admin import UserAdmin
from django.contrib import admin

from .models.models import UserManage, Client, Product, Sale, Detail


class UserAdmin(UserAdmin):
    pass


admin.site.register(UserManage, UserAdmin)
admin.site.register(Client)
admin.site.register(Product)
admin.site.register(Sale)
admin.site.register(Detail)
