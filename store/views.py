from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Sum
from django.http import JsonResponse, HttpResponse
from django.utils import timezone
import datetime
from django.views.decorators.csrf import csrf_exempt
from requests import Response
from rest_framework.parsers import JSONParser

from .serializers import *
from .models.models import *
from .services import SaleValidateService
from .services import TokenService
from .services import SaleClientNotInformed, SaleValueTotalError, SaleClientNotExists

from django.contrib.auth import authenticate
from rest_framework import status, viewsets, generics


@csrf_exempt
def login(request):
    """
    Realize login for user with credentials.
    """
    if request.method == 'POST':
        data = JSONParser().parse(request)

        email = data.get("email")
        password = data.get("password")

        user = authenticate(email=email, password=password)
        if user:
            return JsonResponse({"token": user.auth_token.key}, status=status.HTTP_200_OK)
        else:
            return JsonResponse({"error": "Wrong Credentials"}, status=status.HTTP_400_BAD_REQUEST)


@csrf_exempt
def user_list(request):
    """
    Create a new user.
    """
    if request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = UserSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            authenticate(email=serializer.instance.email, password=serializer.instance.password)
            token, created = Token.objects.get_or_create(user=serializer.instance)
            return JsonResponse({"token": token.key}, status=status.HTTP_201_CREATED)
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@csrf_exempt
def product_list(request):
    """
    List all code products, or create a new product.
    """

    if TokenService.validate_token(request) is None:
        return JsonResponse({'error': "Token is not valid"}, status=status.HTTP_400_BAD_REQUEST)

    if request.method == 'GET':
        products = Product.objects.all()
        serializer = ProductSerializer(products, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = ProductSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@csrf_exempt
def product_detail(request, pk):
    """
    Retrieve, update or delete a code product.
    """
    try:
        product = Product.objects.get(pk=pk)
    except Product.DoesNotExist:
        return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ProductSerializer(product)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = ProductSerializer(product, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        product.delete()
        return HttpResponse(status=status.HTTP_204_NO_CONTENT)


class SaleList(generics.ListCreateAPIView):
    serializer_class = SaleSerializer

    def post(self, request, *args, **kwargs):
        """
            Create new sale and details
        """

        try:

            data = SaleValidateService.validate(request.data)

            serializer = SaleSerializer(data=data)

            if serializer.is_valid(raise_exception=True):
                sale = serializer.save()
                return JsonResponse({'sale': sale.id}, status=status.HTTP_201_CREATED)
            return JsonResponse(status=status.HTTP_400_BAD_REQUEST)

        except SaleClientNotExists:
            return JsonResponse({ 'error': 'Client not exists'}, status=status.HTTP_404_NOT_FOUND)
        except SaleClientNotInformed:
            return JsonResponse({'error': 'Client not informed'}, status=status.HTTP_400_BAD_REQUEST)
        except SaleValueTotalError:
            return JsonResponse({'error': 'Value total diverge of sum sale''s itens'},
                                status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, *args, **kwargs):
        """
            Retrieve all sale and details
        """
        if TokenService.validate_token(request) is None:
            return JsonResponse({'error': "Token is not valid"}, status=status.HTTP_400_BAD_REQUEST)

        sales = Sale.objects.all()
        serializer = SaleSerializer(sales, many=True)
        return JsonResponse(serializer.data, safe=False)


def commission_seller__by_period(request):

    data = JSONParser().parse(request)

    seller = data['id_seller']
    start_date = data['start_date']
    end_date = data['end_date']

    if request.method == 'GET':

        try:
            commissions = Sale.objects.filter(seller=seller,
                                              created__date__range=(start_date,
                                                                    end_date)).aggregate(Sum('total_commission'))

            total_commission = commissions['total_commission__sum']
            if total_commission is None:
                total_commission = 0
            seller_object = Seller.objects.get(id=seller)
            name_seller = seller_object.name

        except ObjectDoesNotExist:
            return JsonResponse({'error': "Does not exist"}, status=status.HTTP_404_NOT_FOUND)

        return JsonResponse({
            'name_seller': name_seller,
            'total_commission': total_commission
        }, status=status.HTTP_200_OK)


@csrf_exempt
def seller_list(request):
    """
    List create a new seller.
    """

    if TokenService.validate_token(request) is None:
        return JsonResponse({'error': "Token is not valid"}, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = SellerSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)