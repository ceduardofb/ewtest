from django.urls import path
from . import views

urlpatterns = [
    path('login/', views.login, name="login"),
    path('user/', views.user_list, name="user_list"),
    path('products/', views.product_list, name="products_list"),
    path('seller/', views.seller_list, name="sellers_list"),
    path('products/<int:pk>/', views.product_detail, name="products_detail"),
    path('sale/', views.SaleList.as_view(), name="sale_list"),
    path('report/commission/', views.commission_seller__by_period, name="commission_by_period"),
]
