from datetime import datetime
import pytest
from django.test import TestCase
from .models.models import Product, UserManage, Client
from .services import SaleValidateService, SaleValueTotalError, ProductDoesNotExists


class ServiceValidateTestCase(TestCase):
    """Test suite for the classes of Services"""

    def setUp(self):
        """Define the test client and other test variables."""

        user = UserManage(
            email="email@email.com",
            username="user_test"
        )
        user.set_password("123456")
        user.save()

        self.client = Client.objects.create(user=user, name="Teste", address="", complement="", city="", state="",
                                            phone="")

        self.product, created = Product.objects.get_or_create(description="PRODUCT", abstract="PRODUCT  test",
                                                              value=15, stock=0, percent_commission=10)

        self.request = {"client": self.client.id, "seller": "", "comments": "Comments", "details": [{"quantity": 1,
                                                                                             "product": self.product.id,
                                                                                             "value": 10,
                                                                                             "comments": ""}],
                        "total_value": 10}

    def test_service_validate_ok(self):

        return_method = SaleValidateService.validate(self.request)

        return_validated = {"client": self.client.id, "seller": "", "comments": "Comments",
                            "details": [{"quantity": 1, "product": self.product.id, "value": 10, "comments": "",
                                         "commission": 1.0}],
                            "total_value": 10, 'total_commission': 1.0}

        self.assertEqual(return_method, return_validated)

    def test_service_value_total_error(self):

        request = {"client": self.client.id, "seller": "", "comments": "Comments",
                   "details": [{"quantity": 1, "product": self.product.id, "value": 10, "comments": "",
                                "commission": 1.0}], "total_value": 11, 'total_commission': 1.0}

        with pytest.raises(SaleValueTotalError):
            assert SaleValidateService.validate(request)


class ServiceCalculateCommissionTestCase(TestCase):
    """Test suite for the classes of Services"""

    def setUp(self):
        """Define the test client and other test variables."""
        self.product, created = Product.objects.get_or_create(description="PRODUCT", abstract="PRODUCT  test",
                                                              value=100, stock=0, percent_commission=10)

    def test_commission_first_part_day_over_5(self):

        self.product.percent_commission = 10.0
        self.product.save()

        time = datetime.strptime('09::30::00', '%H::%M::%S').time()
        value = 100.0
        product = self.product.id

        commission = SaleValidateService.calculate_commission(time, value, product)

        self.assertEqual(commission, 5.0)

    def test_commission_first_part_day_below_5(self):
        self.product.percent_commission = 2.0
        self.product.save()

        time = datetime.strptime('09::30::00', '%H::%M::%S').time()
        value = 100.0
        product = self.product.id

        commission = SaleValidateService.calculate_commission(time, value, product)

        self.assertEqual(commission, 2.0)

    def test_commission_second_part_day_over_4(self):

        self.product.percent_commission = 10.0
        self.product.save()

        time = datetime.strptime('14::30::00', '%H::%M::%S').time()
        value = 100.0
        product = self.product.id

        commission = SaleValidateService.calculate_commission(time, value, product)

        self.assertEqual(commission, 10.0)

    def test_commission_second_part_day_below_4(self):

        self.product.percent_commission = 2.0
        self.product.save()

        time = datetime.strptime('14::30::00', '%H::%M::%S').time()
        value = 100.0
        product = self.product.id

        commission = SaleValidateService.calculate_commission(time, value, product)

        self.assertEqual(commission, 4.0)

    def test_commission_product_not_exist(self):

        time = datetime.strptime('14::30::00', '%H::%M::%S').time()
        value = 100.0
        product = 25

        with pytest.raises(ProductDoesNotExists):
            assert SaleValidateService.calculate_commission(time, value, product)