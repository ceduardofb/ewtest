import datetime

from rest_framework.authtoken.models import Token
from .models.models import Product, Client



"""

This classes, implements a business logic. 
All rules are placed here, leaving the classes and methods in the view just to perform the role of controller.

"""


class TokenInvalidError(BaseException):
    pass


class TokenDoesNotExists(BaseException):
    pass


class SaleValueTotalError(BaseException):
    pass


class SaleClientNotInformed(BaseException):
    pass


class SaleClientNotExists(BaseException):
    pass


class ProductDoesNotExists(BaseException):
    pass


class TokenService(object):

    @staticmethod
    def validate_token(request):
        httpauthorization = request.META.get('HTTP_AUTHORIZATION')

        if httpauthorization is not None:
            try:
                strtoken = httpauthorization.lower().lstrip('token').replace(" ", "")
                token = Token.objects.get(key=strtoken)
                if token is not None:
                    return token
                else:
                    raise TokenInvalidError
            except Token.DoesNotExist:
                raise TokenDoesNotExists
        else:
            raise TokenInvalidError


class SaleValidateService(object):

    @staticmethod
    def calculate_commission(time, value, id_product):
        """
        Method do calculate commission of sale's item. Validate the hour of sale to apply rule of commission
        :param time: time of sale
        :param value: value of item of sale
        :param product: product's id
        :return: value
        """

        try:

            product = Product.objects.get(id=id_product)

            percent_commission = product.percent_commission

            if datetime.time(0, 0, 0) <= time <= datetime.time(12, 0, 0) and percent_commission > 5:
                percent_commission = 5.0

            if datetime.time(12, 0, 0) < time < datetime.time(23, 59, 59) and percent_commission < 4:
                percent_commission = 4.0

            commission = value * (percent_commission / 100)

            return commission

        except Product.DoesNotExist:
            raise ProductDoesNotExists

    @staticmethod
    def validate(request):
        """
        Method to validate business rules
        :param request:
        :return: data (request)
        """
        time = datetime.datetime.now().time()

        data = request

        total_commission = 0.0
        total = 0.0

        if data['client'] is None or data['client'] == "":
            raise SaleClientNotInformed

        if not Client.objects.filter(id=data['client']).exists():
            raise SaleClientNotExists

        for i in range(0, len(data['details'])):
            commission = SaleValidateService.calculate_commission(time, data['details'][i]['value'],
                                                                  data['details'][i]['product'])
            data['details'][i]['commission'] = commission
            total_commission += commission
            total += data['details'][i]['value']

        if total != data['total_value']:
            raise SaleValueTotalError

        data['total_commission'] = total_commission

        return data
