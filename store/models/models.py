from django.db import models
from store.models.manager import *
from django.contrib.auth.models import AbstractUser


# Create your models here.
class UserManage(AbstractUser):

    class Meta:
        app_label = "store"

    USERNAME_FIELD = 'email'
    email = models.EmailField('email address', unique=True)
    username = models.CharField(max_length=20, null=True, blank=True)

    REQUIRED_FIELDS = ['username']  # removes email from REQUIRED_FIELDS

    def __str__(self):
        return self.email

    def create_superuser(self, email, password=None, **extra_fields):
        if not email:
            raise ValueError("User must have an email")
        if not password:
            raise ValueError("User must have a password")

        user = self.model(
            email=self.normalize_email(email)
        )
        user.set_password(password)
        user.username = ''
        user.admin = True
        user.staff = True
        user.active = True
        user.save(using=self._db)
        return user


class Client(SoftDeletionModel):

    class Meta:
        app_label = "store"

    user = models.OneToOneField(UserManage, on_delete=models.CASCADE, related_name="client")
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=250)
    complement = models.CharField(max_length=150, blank=True, null=True, default=None)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    phone = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Seller(SoftDeletionModel):
    name = models.CharField(max_length=200)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Product(SoftDeletionModel):

    class Meta:
        app_label = "store"

    description = models.CharField(max_length=100)
    abstract = models.TextField(max_length=400, blank=True)
    value = models.DecimalField(max_digits=6, decimal_places=2, blank=True)
    stock = models.IntegerField(blank=True, default=0)
    percent_commission = models.FloatField(blank=True, default=0, null=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.description


class Sale(SoftDeletionModel):

    class Meta:
        app_label = "store"

    NEW = "NEW"
    PAID = "PAID"
    DELIVERED = "DELIVERED"
    CANCEL = "CANCEL"
    STATUS_CHOICES = (
        (NEW, 'New'),
        (PAID, 'Paid'),
        (DELIVERED, 'Delivered'),
        (CANCEL, 'Cancel')
    )

    client = models.ForeignKey(Client, on_delete=models.PROTECT, related_name="clients")
    seller = models.ForeignKey(Seller, on_delete=models.DO_NOTHING, null=True, blank=True, related_name="seller")
    comments = models.TextField(max_length=500, blank=True, null=True)
    date = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=50, choices=STATUS_CHOICES, default=NEW)
    total_commission = models.DecimalField(max_digits=6, decimal_places=2, blank=True, default=0, null=True)
    total_value = models.DecimalField(max_digits=6, decimal_places=2, blank=True, default=0, null=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)


class Detail(models.Model):

    class Meta:
        app_label = "store"

    sale = models.ForeignKey(Sale, on_delete=models.PROTECT, related_name="details")
    product = models.ForeignKey(Product, on_delete=models.PROTECT, related_name="products")
    quantity = models.IntegerField(default=0)
    value = models.DecimalField(max_digits=6, decimal_places=2, blank=True, default=0)
    commission = models.DecimalField(max_digits=6, decimal_places=2, blank=True, default=0, null=True)
    comments = models.TextField(max_length=500, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)