from rest_framework import serializers
from rest_framework.authtoken.models import Token
from .models.models import UserManage, Client, Product, Sale, Detail, Seller
from django.db import transaction


class ClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Client
        fields = ('name', 'address', 'complement', 'city', 'state', 'phone')


class UserSerializer(serializers.ModelSerializer):
    client = ClientSerializer(many=False)

    class Meta:
        model = UserManage
        fields = ('password', 'email', 'client')
        extra_kwargs = {'password': {'write_only': True}}

    @transaction.atomic
    def create(self, validated_data):

        email = validated_data['email']
        username = email.split('@')[0]

        name = validated_data['client']['name']
        address = validated_data['client']['address']
        complement = validated_data['client']['complement']
        city = validated_data['client']['city']
        state = validated_data['client']['state']
        phone = validated_data['client']['phone']

        user = UserManage(
            email=email,
            username=username
        )
        user.set_password(validated_data['password'])
        user.save()

        Client.objects.create(user=user, name=name, address=address, complement=complement, city=city,
                              state=state, phone=phone)

        Token.objects.create(user=user)
        return user


class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ('id', 'description', 'value', 'stock', 'percent_commission')
        extra_kwargs = {'id': {'read_only': True}}


class DetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = Detail
        fields = ('id', 'quantity', 'value', 'comments', 'product', 'commission')
        extra_kwargs = {'id': {'read_only': True}}


class SaleSerializer(serializers.ModelSerializer):
    details = DetailSerializer(many=True)

    class Meta:
        model = Sale
        fields = ("id", "client", "comments", "details", "seller", "total_commission", "total_value")
        extra_kwargs = {'id': {'read_only': True},
                        'time_sale': {'write_only': True}
                        }

    @transaction.atomic
    def create(self, validated_data):
        details_data = validated_data.pop('details')
        comments = validated_data.pop('comments')
        client = validated_data.pop('client')
        seller = validated_data.pop('seller')
        total_commission = validated_data.pop('total_commission')
        total_value = validated_data.pop('total_value')

        sale = Sale.objects.create(comments=comments, client=client, seller=seller, total_commission=total_commission,
                                   total_value=total_value)

        for detail_data in details_data:
            product = detail_data.pop('product')
            quantity = detail_data.pop('quantity')
            value = detail_data.pop('value')
            comments = detail_data.pop('comments')
            commission = detail_data.pop('commission')

            Detail.objects.create(sale=sale, product=product, quantity=quantity, value=value, comments=comments,
                                  commission=commission)

        return sale


class SellerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Seller
        fields = '__all__'
